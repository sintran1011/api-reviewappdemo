/* Import database */
const models = require("../database/models");
const database = require("../database");

const getAllReviews = () => {
  try {
    return models.Reviews.findAll({
      order: [["createdAt", "DESC"]],
    });
  } catch (error) {
    throw error;
  }
};

const averageRating = (productId) => {
  try {
    return models.Reviews.findAll({
      where: { productId, state: true },
      attributes: ['rating'],
      raw: true
    })
  } catch (error) {
    throw error;
  }
}

const countReviewByState = (productId) => {
  try {
    return models.Reviews.count({
      where: { state: true, productId },
    });
  } catch (error) {
    throw error;
  }
};

const getAllReviewByProductId = (productId, limit, offset) => {
  try {
    return models.Reviews.findAll({
      limit,
      offset,
      where: { productId, state: true },
      order: [["createdAt", "DESC"]],
      include: [{ model: models.Replies }],
    });
  } catch (error) {
    throw error;
  }
};

const createReview = async (data) => {
  try {
    const {
      productId,
      author,
      content,
      email,
      rating,
      review_title,
      productImg,
      productTitle,
    } = data;
    const createdReview = await models.Reviews.create({
      productId,
      author,
      content,
      email,
      rating,
      review_title,
      productImg,
      productTitle,
    });
    return createdReview;
  } catch (error) {
    throw error;
  }
};

const updateReview = async (state, reviewId) => {
  try {
    const updatedReview = await models.Reviews.update(
      {
        state,
      },
      { where: { reviewId } }
    );
    return updatedReview[0];
  } catch (error) {
    throw error;
  }
};

const deleteReview = async (reviewId) => {
  try {
    return await models.Reviews.destroy({
      where: { reviewId },
    });
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getAllReviews,
  countReviewByState,
  getAllReviewByProductId,
  createReview,
  updateReview,
  deleteReview,
  averageRating
};
