/* Import database */
const models = require("../database/models");
const database = require("../database");

const getRepliesById = (reviewId) => {
  try {
    return models.Replies.findAll({
      where: { reviewId },
      order: [[ 'createdAt', 'ASC' ]]
    })
  } catch (error) {
    throw error;
  }
}

const createReply = async (data) => {
  try {
    const { role, content, reviewId } = data;
    const createdReply = await models.Replies.create({
      reviewId,
      role,
      content
    })
    return createdReply;
  } catch (error) {
    throw error;
  }
}

module.exports = {
  getRepliesById,
  createReply
}