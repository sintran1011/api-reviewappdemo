'use strict';
const {v4: uuidv4} = require('uuid');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Replies extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Replies.belongsTo(models.Reviews, {
        foreignKey: "reviewId"
      })
    }
  }
  Replies.init({
    replyId: {
      type: DataTypes.UUID(36),
      primaryKey: true,
      allowNull: false,
      defaultValue: () => uuidv4(),
    },
    reviewId: {
      type: DataTypes.UUID(36),
      foreignKey: true,
      allowNull: false,
    },
    content: DataTypes.STRING,
    role: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  }, {
    sequelize,
    modelName: 'Replies',
  });
  return Replies;
};