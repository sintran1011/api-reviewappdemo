'use strict';
const {v4: uuidv4} = require('uuid');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reviews extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Reviews.hasMany(models.Replies, {
        foreignKey: "reviewId",
        sourceKey: "reviewId"
      })
    }
  }
  Reviews.init({
    reviewId: {
      type: DataTypes.UUID(36),
      primaryKey: true,
      allowNull: false,
      defaultValue: () => uuidv4(),
    },
    productId: {
      type: DataTypes.UUID(36),
      foreignKey: true,
      allowNull: false,
    },
    productImg: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    productTitle: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    author: DataTypes.STRING,
    content: DataTypes.STRING,
    email: DataTypes.STRING,
    state: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    rating: DataTypes.INTEGER,
    review_title: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Reviews',
  });
  return Reviews;
};