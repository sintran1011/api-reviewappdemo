'use strict';
const {v4: uuidv4} = require('uuid');
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Replies', {
      replyId: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID(36),
        defaultValue: uuidv4(),
      },
      reviewId: {
        type: Sequelize.UUID(36),
        allowNull: false,
        foreignKey: true,
      },
      content: {
        type: Sequelize.STRING
      },
      role: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Replies');
  }
};