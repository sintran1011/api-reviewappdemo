"use strict";
const { v4: uuidv4 } = require("uuid");
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Reviews", {
      reviewId: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID(36),
        defaultValue: uuidv4(),
      },
      productId: {
        type: Sequelize.UUID(36),
        allowNull: false,
        foreignKey: true,
      },
      productImg: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      productTitle: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      author: {
        type: Sequelize.STRING,
      },
      content: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
      state: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      rating: {
        type: Sequelize.INTEGER,
      },
      review_title: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Reviews");
  },
};
