'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addConstraint('replies', {
      type: "foreign key",
      name: "fkey_constraint_replies_reviews_reviewId",
      fields: ['reviewId'],
      references: {
        //Required field
        table: "reviews",
        field: "reviewId"
      },
      onDelete: "cascade",
      onUpdate: "cascade"
    })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeConstraint(
      'replies',
      'fkey_constraint_replies_reviews_reviewId'
    )
  }
};
