/* Import packages */
const Sequelize = require("sequelize");
/* Import env variables */
const nodeEnv = process.env.NODE_ENV ? process.env.NODE_ENV : "test";
/* Import connection */
const connection = require("./connection")[nodeEnv];
/* Init sequelize instance */
let sequelize;

if (!connection) {
  sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
      host: process.env.DB_HOST,
      dialect: "mysql",
    }
  );
} else {
  /* Get db connection info */
  const {
    database: dbName,
    username: dbUsername,
    password: dbPassword,
    host: dbHost,
  } = connection;

  sequelize = new Sequelize(dbName, dbUsername, dbPassword, {
    host: dbHost,
    dialect: "mysql",
  });
}

/* Test database connection */
const testConnection = () => {
  return sequelize
    .authenticate()
    .then(() => {
      console.log(
        "[✨ Database] Connection has been established successfully."
      );
    })
    .catch((err) => {
      console.error("[❌ Database] Unable to connect to the database:", err);
    });
};

/* Export */
module.exports = {
  sequelize,
  testConnection,
};