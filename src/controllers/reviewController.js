const _ = require("lodash");
const reviewService = require("../services/reviewService");

const getAllReviews = async (req, res, next) => {
  try {
    const reviews = await reviewService.getAllReviews();
    return res.status(200).json({ reviews });
  } catch (error) {
    console.log("error", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

const getAllReviewsByProductId = async (req, res, next) => {
  try {
    const { id: productId } = req.params;
    const { page, limit } = req.query;
    let pageAsNumber = 1;
    let limitAsNumber = 5;
    if (!Number.isNaN(Number(page)) && Number(page) > 0) {
      pageAsNumber = Number(page);
    }
    if (!Number.isNaN(Number(limit)) && Number(limit) > 0) {
      limitAsNumber = Number(limit);
    }
    const offset = limitAsNumber * (pageAsNumber - 1);
    const count = await reviewService.countReviewByState(productId);
    const reviews = await reviewService.getAllReviewByProductId(
      productId,
      limitAsNumber,
      offset
    );
    const rating = await reviewService.averageRating(productId);
    const rate = rating.map((rate) => rate.rating);
    const sum = rate.reduce((a, b) => a + b, 0);
    const avg = sum / rate.length || 0;
    if (!reviews) {
      return res.status(404).json({ message: "Reviews don't exist" });
    }
    return res.status(200).json({ page: pageAsNumber, count, avg, reviews });
  } catch (error) {
    console.log("error", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

const createReview = async (req, res, next) => {
  try {
    const {
      productId,
      author,
      content,
      email,
      rating,
      review_title,
      productImg,
      productTitle,
    } = req.body;
    if (_.isEmpty(productId)) {
      return res.status(400).json({ message: "Missing productId" });
    }
    if (_.isEmpty(author)) {
      return res.status(400).json({ message: "Missing author" });
    }
    if (_.isEmpty(content)) {
      return res.status(400).json({ message: "Missing content" });
    }
    if (_.isEmpty(email)) {
      return res.status(400).json({ message: "Missing email" });
    }
    if (_.isEmpty(review_title)) {
      return res.status(400).json({ message: "Missing review_title" });
    }
    if (_.isEmpty(productImg)) {
      return res.status(400).json({ message: "Missing image" });
    }
    if (_.isEmpty(productTitle)) {
      return res.status(400).json({ message: "Missing product_title" });
    }
    const inputData = {
      productId,
      author,
      content,
      email,
      rating,
      review_title,
      productImg,
      productTitle,
    };
    const createdReview = await reviewService.createReview({ ...inputData });
    return res.status(200).json({ createdReview });
  } catch (error) {
    console.log("error", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

const updatedReview = async (req, res, next) => {
  try {
    const { id: reviewId } = req.params;
    const { state } = req.body;
    console.log("state", state);
    const updatedReview = await reviewService.updateReview(state, reviewId);
    console.log("updatedReview", updatedReview);
    if (updatedReview !== 1) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
    return res.sendStatus(202);
  } catch (error) {
    console.log("error", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

const deleteReview = async (req, res, next) => {
  try {
    const { id: reviewId } = req.params;
    const deletedReview = await reviewService.deleteReview(reviewId);
    return res.status(200).json({ deletedReview });
  } catch (error) {
    console.log("error", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

module.exports = {
  getAllReviews,
  getAllReviewsByProductId,
  createReview,
  updatedReview,
  deleteReview,
};
