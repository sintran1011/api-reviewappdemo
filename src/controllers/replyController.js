const _ = require('lodash');
const replyService = require('../services/replyService')

const getRepliesById = async (req, res, next) => {
  try {
    const {id: reviewId} = req.params;
    const replies = await replyService.getRepliesById(reviewId);
    if (!replies) {
      return res.status(404).json({ message: "Replies don't exist" })
    }
    return res.status(200).json({replies});
  } catch (error) {
    console.log("error", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
}

const createReply = async (req, res, next) => {
  try {
    const {reviewId, role, content} = req.body;
    const inputData = {
      role,
      content,
      reviewId
    }
    const createdReply = await replyService.createReply(inputData);
    return res.status(200).json(createdReply);
  } catch (error) {
    console.log("error", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
}

module.exports = {
  getRepliesById,
  createReply
}