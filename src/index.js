/* Dotenv config */
require("dotenv").config();

/* Import packages */
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

/* Database */
const database = require("./database");

/* Routes */
const reviewRoute = require("./routes/reviewRoute");
const replyRoute = require("./routes/replyRoute");

/* App config */
const app = express();
const port = process.env.APP_PORT;
database.testConnection();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "https://sintranstore.myshopify.com");

  next();
});

const corsOptions = {
  optionSuccessStatus: 200,
};

/* Cors & body-parser config */
app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/reviews", reviewRoute);
app.use("/replies", replyRoute);
/* App.listen */
app.listen(port, () =>
  console.log(`[🚀 Server] App listening on port ${port}!`)
);
