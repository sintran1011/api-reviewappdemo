/* Import packages*/
const express = require("express");
/* Init router */
const router = express.Router();

const replyController = require("../controllers/replyController");

router.post('/', replyController.createReply)
router.get('/:id', replyController.getRepliesById);

module.exports = router;