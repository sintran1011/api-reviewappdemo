/* Import packages*/
const express = require("express");

const reviewController = require("../controllers/reviewController")

/* Init router */
const router = express.Router();

router.post('/', reviewController.createReview);
router.get('/', reviewController.getAllReviews);
router.get('/:id', reviewController.getAllReviewsByProductId);
router.patch('/:id', reviewController.updatedReview);
router.delete('/:id', reviewController.deleteReview);

module.exports = router;